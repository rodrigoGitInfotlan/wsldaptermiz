package com.termiz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsLdapTermizApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsLdapTermizApplication.class, args);
	}

}
