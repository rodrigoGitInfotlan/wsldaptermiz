package com.termiz.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class ResponseAuthLDAP {
	private String estatus;
	private String descripcion;
	private String timestamp;
		
	public ResponseAuthLDAP(String estatus, String descripcion, String claveOTP) {
		super();
		this.estatus = estatus;
		this.descripcion = descripcion;
		this.timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Timestamp(System.currentTimeMillis()));
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
}
