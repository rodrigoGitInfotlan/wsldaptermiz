package com.termiz.request;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AppController {
	@RequestMapping(value = "/login")
    public String login() {
		System.out.println("AppController.login");
        return "signin";
    }
}
