package com.termiz.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.termiz.model.ResponseAuthLDAP;


@RestController
public class HomeResource {
	@GetMapping("/")
	public String index() {
		System.out.println("HomeResource.index");
		return "Home page";
	}
	
	@RequestMapping(value = "/test", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<ResponseAuthLDAP> vista(){
		System.out.println("test");
		ResponseAuthLDAP respuestaOTP=new ResponseAuthLDAP("0","exito","");		
		return new ResponseEntity<ResponseAuthLDAP>(respuestaOTP,HttpStatus.OK);
	}
}
